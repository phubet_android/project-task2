package com.bb.projecttask2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.txtCorrect
import kotlinx.android.synthetic.main.activity_plus_game.*

object GlobalVariablePlus {
    var correct = 0
    var incorrect = 0
}

class PlusGameActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_plus_game)
        txtCorrect.text = "ถูก : "+"${GlobalVariablePlus.correct}"
        txtIncorrect.text = "ผิด : "+"${GlobalVariablePlus.incorrect}"
        generateQuestions()
    }

    private fun generateQuestions() {
        val num1 = findViewById<TextView>(R.id.plus_num1)
        val num2 = findViewById<TextView>(R.id.plus_num2)
        val randomNum1 = (0..10).random()
        val randomNum2 = (0..10).random()
        num1.text = randomNum1.toString()
        num2.text = randomNum2.toString()
        val ans = randomNum1 + randomNum2
        generateAnswer(ans)
    }

    private fun generateAnswer(ans: Int) {
        val answers = arrayOf(
            "btnAns1", "btnAns2", "btnAns3"
        )
        val btnRandom = answers[(0..2).random()]
        if (btnRandom == "btnAns1") {
            plus_btnAns1.text = ans.toString()
            plus_btnAns2.text = (0..20).random().toString()
            plus_btnAns3.text = (0..20).random().toString()
        } else if (btnRandom == "btnAns2") {
            plus_btnAns1.text = (0..20).random().toString()
            plus_btnAns2.text = ans.toString()
            plus_btnAns3.text = (0..20).random().toString()
        } else {
            plus_btnAns1.text = (0..20).random().toString()
            plus_btnAns2.text = (0..20).random().toString()
            plus_btnAns3.text = ans.toString()
        }
        checkAnswer(ans)
    }

    private fun checkAnswer(ans: Int) {
        checkButton1(ans)
        checkButton2(ans)
        checkButton3(ans)
    }

    private fun checkButton1(ans: Int) {
        plus_btnAns1.setOnClickListener {
            val checkAns = plus_btnAns1.text.toString()
            if (checkAns.toInt() == ans) {
                Toast.makeText(MainActivity@ this, "ถูกต้อง", Toast.LENGTH_LONG).show()
                GlobalVariablePlus.correct++
                txtCorrect.text = "ถูก : "+"${GlobalVariablePlus.correct}"
            } else {
                Toast.makeText(MainActivity@ this, "ไม่ถูกต้อง", Toast.LENGTH_LONG).show()
                GlobalVariablePlus.incorrect++
                txtIncorrect.text = "ผิด : "+"${GlobalVariablePlus.incorrect}"
            }
            generateQuestions()
        }
    }

    private fun checkButton2(ans: Int) {
        plus_btnAns2.setOnClickListener {
            val checkAns = plus_btnAns2.text.toString()
            if (checkAns.toInt() == ans) {
                Toast.makeText(MainActivity@ this, "ถูกต้อง", Toast.LENGTH_LONG).show()
                GlobalVariablePlus.correct++
                txtCorrect.text = "ถูก : "+"${GlobalVariablePlus.correct}"
            } else {
                Toast.makeText(MainActivity@ this, "ไม่ถูกต้อง", Toast.LENGTH_LONG).show()
                GlobalVariablePlus.incorrect++
                txtIncorrect.text = "ผิด : "+"${GlobalVariablePlus.incorrect}"
            }
            generateQuestions()
        }
    }

    private fun checkButton3(ans: Int) {
        plus_btnAns3.setOnClickListener {
            val checkAns = plus_btnAns3.text.toString()
            if (checkAns.toInt() == ans) {
                Toast.makeText(MainActivity@ this, "ถูกต้อง", Toast.LENGTH_LONG).show()
                GlobalVariablePlus.correct++
                txtCorrect.text = "ถูก : "+"${GlobalVariablePlus.correct}"
            } else {
                Toast.makeText(MainActivity@ this, "ไม่ถูกต้อง", Toast.LENGTH_LONG).show()
                GlobalVariablePlus.incorrect++
                txtIncorrect.text = "ผิด : "+"${GlobalVariablePlus.incorrect}"
            }
            generateQuestions()
        }
    }

    @Override
    override fun onBackPressed(){
        super.onBackPressed()
        val intent = Intent(PlusGameActivity@this, MainActivity::class.java)
        intent.putExtra("correct", GlobalVariableMinus.correct + GlobalVariablePlus.correct + GlobalVariableMulti.correct)
        intent.putExtra("incorrect", GlobalVariableMinus.incorrect + GlobalVariablePlus.incorrect + GlobalVariableMulti.incorrect)
        startActivity(intent)
    }
}