package com.bb.projecttask2

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btnPlus = findViewById<Button>(R.id.btnPlus)
        btnPlus.setOnClickListener {
            val intent = Intent(MainActivity@this, PlusGameActivity::class.java)
            startActivity(intent)
        }

        val btnMinus = findViewById<Button>(R.id.btnMinus)
        btnMinus.setOnClickListener {
            val intent = Intent(MainActivity@this, MinusGameActivity::class.java)
            startActivity(intent)
        }

        val btnMultiply = findViewById<Button>(R.id.btnMultiply)
        btnMultiply.setOnClickListener {
            val intent = Intent(MainActivity@this, MultiplyGameActivity::class.java)
            startActivity(intent)
        }

        val correct:Int = intent.getIntExtra("correct", 0)
        val incorrect:Int = intent.getIntExtra("incorrect", 0)
        val txtCorrect = findViewById<TextView>(R.id.txtCorrect)
        val txtIncorrect = findViewById<TextView>(R.id.txtIncorrect)
        txtCorrect.text = "ถูก : $correct"
        txtIncorrect.text = "ผิด : $incorrect"
    }
}