package com.bb.projecttask2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_minus_game.*
import kotlinx.android.synthetic.main.activity_multiply_game.*

object GlobalVariableMulti {
    var correct = 0
    var incorrect = 0
}

class MultiplyGameActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_multiply_game)
        multi_txtCorrect.text = "ถูก : "+"${GlobalVariableMulti.correct}"
        multi_txtIncorrect.text = "ผิด : "+"${GlobalVariableMulti.incorrect}"
        generateQuestions()
    }

    private fun generateQuestions() {
        val num1 = findViewById<TextView>(R.id.multi_num1)
        val num2 = findViewById<TextView>(R.id.multi_num2)
        val randomNum1 = (1..10).random()
        val randomNum2 = (1..10).random()
        num1.text = randomNum1.toString()
        num2.text = randomNum2.toString()
        val ans = randomNum1 * randomNum2
        generateAnswer(ans)
    }

    private fun generateAnswer(ans: Int) {
        val answers = arrayOf(
            "btnAns1", "btnAns2", "btnAns3"
        )
        val btnRandom = answers[(0..2).random()]
        if (btnRandom == "btnAns1") {
            multi_btnAns1.text = ans.toString()
            multi_btnAns2.text = (0..100).random().toString()
            multi_btnAns3.text = (0..100).random().toString()
        } else if (btnRandom == "btnAns2") {
            multi_btnAns1.text = (0..100).random().toString()
            multi_btnAns2.text = ans.toString()
            multi_btnAns3.text = (0..100).random().toString()
        } else {
            multi_btnAns1.text = (0..100).random().toString()
            multi_btnAns2.text = (0..100).random().toString()
            multi_btnAns3.text = ans.toString()
        }
        checkAnswer(ans)
    }

    private fun checkAnswer(ans: Int) {
        checkButton1(ans)
        checkButton2(ans)
        checkButton3(ans)
    }

    private fun checkButton1(ans: Int) {
        multi_btnAns1.setOnClickListener {
            val checkAns = multi_btnAns1.text.toString()
            if (checkAns.toInt() == ans) {
                Toast.makeText(MainActivity@ this, "ถูกต้อง", Toast.LENGTH_LONG).show()
                GlobalVariableMulti.correct++
                multi_txtCorrect.text = "ถูก : "+"${GlobalVariableMulti.correct}"
            } else {
                Toast.makeText(MainActivity@ this, "ไม่ถูกต้อง", Toast.LENGTH_LONG).show()
                GlobalVariableMulti.incorrect++
                multi_txtIncorrect.text = "ผิด : "+"${GlobalVariableMulti.incorrect}"
            }
            generateQuestions()
        }
    }

    private fun checkButton2(ans: Int) {
        multi_btnAns2.setOnClickListener {
            val checkAns = multi_btnAns2.text.toString()
            if (checkAns.toInt() == ans) {
                Toast.makeText(MainActivity@ this, "ถูกต้อง", Toast.LENGTH_LONG).show()
                GlobalVariableMulti.correct++
                multi_txtCorrect.text = "ถูก : "+"${GlobalVariableMulti.correct}"
            } else {
                Toast.makeText(MainActivity@ this, "ไม่ถูกต้อง", Toast.LENGTH_LONG).show()
                GlobalVariableMulti.incorrect++
                multi_txtIncorrect.text = "ผิด : "+"${GlobalVariableMulti.incorrect}"
            }
            generateQuestions()
        }
    }

    private fun checkButton3(ans: Int) {
        multi_btnAns3.setOnClickListener {
            val checkAns = multi_btnAns3.text.toString()
            if (checkAns.toInt() == ans) {
                Toast.makeText(MainActivity@ this, "ถูกต้อง", Toast.LENGTH_LONG).show()
                GlobalVariableMulti.correct++
                multi_txtCorrect.text = "ถูก : "+"${GlobalVariableMulti.correct}"
            } else {
                Toast.makeText(MainActivity@ this, "ไม่ถูกต้อง", Toast.LENGTH_LONG).show()
                GlobalVariableMulti.incorrect++
                multi_txtIncorrect.text = "ผิด : "+"${GlobalVariableMulti.incorrect}"
            }
            generateQuestions()
        }
    }

    @Override
    override fun onBackPressed(){
        super.onBackPressed()
        val intent = Intent(MultiplyGameActivity@this, MainActivity::class.java)
        intent.putExtra("correct",  GlobalVariableMulti.correct + GlobalVariableMinus.correct + GlobalVariablePlus.correct)
        intent.putExtra("incorrect", GlobalVariableMulti.incorrect + GlobalVariableMinus.incorrect + GlobalVariablePlus.incorrect)
        startActivity(intent)
    }
}